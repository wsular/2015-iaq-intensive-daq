Changelog for outdoor AQ DAQFactory program
===========================================

outdoor_20150723
----------------

* Revert changes from "outdoor_20150723-test": determined changes did not
  improve data signal quality


outdoor_20150723-test
---------------------

* Remove zero offset applied to LI-840A carbon dioxide signal.


outdoor_20150707
----------------

* Initial field-deployed version, checked in on 2015 July 23 and ostensibly
  unmodified since deployment on 2015 July 7.

